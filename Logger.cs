﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonCore
{
    public static class Logger
    {
        public static void AddMessage(string msg)
        {
            string log_path = Path.Combine(Paths.CurrentExecutingDirectoryPath, "log.txt");
            StreamWriter sw = new StreamWriter(log_path, true);
            sw.WriteLine(string.Format("{0}|{1}", DateTime.Now.ToString(), msg));
            sw.Close();

        }
    }
}
