﻿using System.Data;
using System.Data.SqlClient;

namespace CommonCore
{
    public static class Data
    {
        public static string GetColumnValue(DataRow dr, string column)
        {
            return dr[column].ToString().Trim();
        }

        public static int FirstQueryResult(string connection_string, string query)
        {
            int res = -1;

            SqlConnection conn = new SqlConnection(connection_string);
            conn.Open();
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.CommandTimeout = 120;
            DataTable t1 = new DataTable();
            using (SqlDataAdapter a = new SqlDataAdapter(cmd))
            {
                a.Fill(t1);
            }
            int.TryParse(t1.Rows[0][0].ToString(), out res);
            return res;

        }
        public static int ExecuteNonQuery(string connection_string, string query)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            conn.Open();
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.CommandTimeout = 120;
            int res = cmd.ExecuteNonQuery();
            return res;
        }
        public static DataTable ExecuteQuery(string connection_string, string query)
        {
            SqlConnection conn = new SqlConnection(connection_string);
            conn.Open();
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.CommandTimeout = 120;
            DataTable t1 = new DataTable();
            using (SqlDataAdapter a = new SqlDataAdapter(cmd))
            {
                a.Fill(t1);
            }
            return t1;
        }

        public static string BuildSQLServerConnectionString(string server, string database, string username, string password)
        {
            return string.Format("Server={0};Database={1};User Id={2};Password={3};",
                server, database, username, password);
        }
    }
}