# CommonCore

Some simple, frequently used functions for my .net core development

## Data
- **ExecuteQuery**: Executes a SQL query and returns a DataTable

## Logger
- **AddMessage**: Writes messages with timestamp to "log.txt" in execution directory.

## OptionFileHandler

Serializes a class representing a list of options into a .DAT file. Used to add and retrieve program options. Accepts any type without casting.
- **AddOption**: Creates/updates an option to the file
```
AddOption("Name", "Jason");
AddOption("Score", 97);
```
- **ReadOption**: Retrieves an option from the file
```
var name = ReadOption("Name");
var score = ReadOption("Score");
```

## Paths

Used to retrieve paths related to current execution

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-2.0)