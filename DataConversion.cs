﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonCore
{
    public static class DataConversion
    {
        public static List<string> DataTableColumnToUniqueStringList(DataTable dataTable, string columnName)
        {
            List<string> res = new List<string>();
            foreach (DataRow dr in dataTable.Rows)
            {
                string val = dr[columnName].ToString();
                if(!res.Contains(val))
                    res.Add(val);
            }
            return res;
        }

        public static string ListOfStringsToQueryParameter(List<String> StringList)
        {
            StringBuilder sb = new StringBuilder();
            foreach (String str in StringList)
            {
                if(sb.Length > 0)
                    sb.Append(",");
                sb.Append("'");
                sb.Append(str);
                sb.Append("'");
            }
            return sb.ToString();
        }

        public static void WriteToCsvFile(this DataTable dataTable, string filePath)
        {
            StringBuilder fileContent = new StringBuilder();

            foreach (var col in dataTable.Columns)
            {
                fileContent.Append(col.ToString() + ",");
            }

            fileContent.Replace(",", System.Environment.NewLine, fileContent.Length - 1, 1);

            foreach (DataRow dr in dataTable.Rows)
            {
                foreach (var column in dr.ItemArray)
                {
                    fileContent.Append("\"" + column.ToString() + "\",");
                }

                fileContent.Replace(",", System.Environment.NewLine, fileContent.Length - 1, 1);
            }

            System.IO.File.WriteAllText(filePath, fileContent.ToString());
        }
    }
}
