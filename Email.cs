﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CommonCore
{
    public static class Email
    {
        public static void Send(string client_address, string username, string password, string from, string to, string body, string subject)
        {
            var client = new SmtpClient(client_address)
            {
                Port = 587,
                UseDefaultCredentials = false,
                EnableSsl = true,
                Credentials = new NetworkCredential(username, password)
            };

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(from);
            foreach(string t in to.Split(";"))
                mailMessage.To.Add(t);
            mailMessage.Body = body;
            mailMessage.Subject = subject;
            mailMessage.IsBodyHtml = true;

            try
            {
                client.Send(mailMessage);
            }
            catch (Exception xx)
            {
                Console.WriteLine(xx.Message);
            }
        }

    }
}
