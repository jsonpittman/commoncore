﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Web;
using System.Xml;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using System.Dynamic;

namespace CommonCore
{
    public static class REST
    {
        public class HeaderList
        {
            public string Name { get; set; }
            public string Value { get; set; }
        }

        public static JArray REST_PUT(string URL, string token, List<HeaderList> headers, string body)
        {
            /*
             * Used to execute a rest GET call and convert result to a datatable
             * 
             */
            DataTable dat = new DataTable();
            HttpWebResponse response;

            WebRequest wrPUTURL;
            wrPUTURL = WebRequest.Create(URL);
            wrPUTURL.Method = "PUT";
            wrPUTURL.Timeout = 20000;
            wrPUTURL.ContentType = "application/json";
            wrPUTURL.Headers.Add("Authorization", string.Format("Bearer {0}", token));
            foreach(var head in headers)
            {
                wrPUTURL.Headers.Add(head.Name, head.Value);
            }
            Stream objStream;
            StreamReader objReader;
            XmlDocument doc = new XmlDocument();
            string err_msg = "OK";


            //string docstr = doc.InnerXml.ToString();

            byte[] bytes = Encoding.UTF8.GetBytes(body);

            wrPUTURL.ContentLength = bytes.Length;

            using (Stream putStream = wrPUTURL.GetRequestStream())
            {
                putStream.Write(bytes, 0, bytes.Length);
            }

            using (response = (HttpWebResponse)wrPUTURL.GetResponse())
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                var res = reader.ReadToEnd();
                doc = new XmlDocument();
                doc.LoadXml(res);
                //new_order_id = doc.SelectSingleNode("/orders").Attributes["id"].InnerText;
                //new_mvt_id = doc.SelectSingleNode("/orders").Attributes["curr_movement_id"].InnerText;

                //post_result.Add(string.Format("Order Created: {0} ({1}) Movement: {2} Driver {3}", new_order_id, dr["recurring_order_id"], new_mvt_id, dr["driver_id"]));
                //CommonCore.Logger.AddMessage(string.Format("Order Created: {0} ({1}) Movement: {2} Driver {3}", new_order_id, dr["recurring_order_id"], new_mvt_id, dr["driver_id"]));
            }



            //try
            //{
            //    objStream = wrPUTURL.GetResponse().GetResponseStream();
            //    objReader = new StreamReader(objStream);
            //    var res = objReader.ReadToEnd();
            //    dynamic blogPosts = JArray.Parse(res);
            //    return blogPosts;
            //    Console.Write(blogPosts[0].blnum);

            //    var splashInfo = JsonConvert.DeserializeObject<dynamic>(res);
            //    doc.Load(objReader);
            //}
            //catch (WebException xx)
            //{
            //    var err_read = xx.Response.GetResponseStream();
            //    using (StreamReader reader = new StreamReader(err_read))
            //    {
            //        var res = reader.ReadToEnd();
            //        err_msg = res;
            //    }
            //    err_msg = HttpUtility.UrlEncode(err_msg);
            //    err_msg = err_msg.Replace("+", "%20");
            //    //return dat;
            //}



            //var xmlReader = new XmlNodeReader(doc);
            //DataSet ds = new DataSet();
            //ds.ReadXml(xmlReader);

            //return ds.Tables[0];
            return null;

        }
        public static JArray REST_GET_To_JArray(string URL, string token, List<HeaderList> headers)
        {
            /*
             * Used to execute a rest GET call and convert result to a datatable
             * 
             */
            DataTable dat = new DataTable();
            HttpWebResponse response;

            WebRequest wrGETURL;
            wrGETURL = WebRequest.Create(URL);
            wrGETURL.Method = "GET";
            wrGETURL.Timeout = 20000;
            wrGETURL.ContentType = "application/json";
            wrGETURL.Headers.Add("Authorization", string.Format("Bearer {0}", token));
            foreach(var head in headers)
            {
                wrGETURL.Headers.Add(head.Name, head.Value);
            }
            Stream objStream;
            StreamReader objReader;
            XmlDocument doc = new XmlDocument();
            string err_msg = "OK";

            try
            {
                objStream = wrGETURL.GetResponse().GetResponseStream();
                objReader = new StreamReader(objStream);
                var res = objReader.ReadToEnd();
                dynamic blogPosts = JArray.Parse(res);
                return blogPosts;
                Console.Write(blogPosts[0].blnum);

                //var splashInfo = JsonConvert.DeserializeObject<dynamic>(res);
                var splashInfo = JsonConvert.DeserializeObject<ExpandoObject>(res, new ExpandoObjectConverter());
                doc.Load(objReader);
            }
            catch (WebException xx)
            {
                var err_read = xx.Response.GetResponseStream();
                using (StreamReader reader = new StreamReader(err_read))
                {
                    var res = reader.ReadToEnd();
                    err_msg = res;
                }
                err_msg = HttpUtility.UrlEncode(err_msg);
                err_msg = err_msg.Replace("+", "%20");
                //return dat;
            }



            //var xmlReader = new XmlNodeReader(doc);
            //DataSet ds = new DataSet();
            //ds.ReadXml(xmlReader);

            //return ds.Tables[0];
            return null;

        }
        public static string CreateToken(string URL, string user_login, List<HeaderList> headers)
        {
            /*
             * Used to create a login token for a secure rest service
             * 
             */
            HttpWebResponse response;

            WebRequest wrGETURL;
            wrGETURL = WebRequest.Create(URL);
            wrGETURL.Method = "POST";
            wrGETURL.Timeout = 20000;
            wrGETURL.ContentType = "application/xml";
            wrGETURL.Headers.Add("Authorization", string.Format("Basic {0}", user_login));
            foreach(var head in headers)
            {
                wrGETURL.Headers.Add(head.Name, head.Value);
            }
            Stream objStream;
            StreamReader objReader;
            XmlDocument doc = new XmlDocument();
            string err_msg = "OK";

            try
            {
                objStream = wrGETURL.GetResponse().GetResponseStream();
                objReader = new StreamReader(objStream);
                return objReader.ReadToEnd();
            }
            catch (WebException xx)
            {
                var err_read = xx.Response.GetResponseStream();
                using (StreamReader reader = new StreamReader(err_read))
                {
                    var res = reader.ReadToEnd();
                    err_msg = res;
                }
                err_msg = HttpUtility.UrlEncode(err_msg);
                err_msg = err_msg.Replace("+", "%20");
                return "";
            }
        }
    }
}
