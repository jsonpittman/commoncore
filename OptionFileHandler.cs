using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;

namespace CommonCore
{
    public static class OptionHandler
    {
        // Usage:
        // CommonCore.OptionHandler.AddOption("Age", 43);
        // CommonCore.OptionHandler.AddOption("Name", "Jason");
        // var my_age = CommonCore.OptionHandler.ReadOption("Age");
        // var my_name = CommonCore.OptionHandler.ReadOption("Name");

        [Serializable]
        private class Option
        {
            public string Name { get; set; }
            public dynamic Value { get; set; }
        }

        private static List<Option> Options = new List<Option>();

        private static string OptionFilePath
        {
            get
            {
                return string.Format("{0}\\{1}.DAT", Paths.CurrentExecutingDirectoryPath, Paths.CurrentExecutingFileName);
            }
        }

        public static void AddOption(string Name, dynamic Value)
        {
            Load();
            var o = Options.Where(op => op.Name == Name).FirstOrDefault();
            if (o == null)
            {
                o = new Option() { Name = Name, Value = Value };
                Options.Add(o);
            }
            else
            {
                o.Value = Value;
            }
            Save(OptionFilePath);
        }

        private static void Load()
        {
            if (File.Exists(OptionFilePath))
            {
                Stream stream = File.Open(OptionFilePath, FileMode.Open);
                BinaryFormatter bformatter = new BinaryFormatter();
                Options = (List<Option>)bformatter.Deserialize(stream);
                stream.Close();
            }
            else
            {
                Options = new List<Option>();
            }
        }

        private static void Save(string ID)
        {
            Stream stream = File.Open(OptionFilePath, FileMode.Create);
            BinaryFormatter bformatter = new BinaryFormatter();
            bformatter.Serialize(stream, Options);
            stream.Close();
        }

        public static dynamic ReadOption(string Name)
        {
            Load();
            var o = Options.Where(op => op.Name == Name).FirstOrDefault();
            if (o != null)
                return o.Value;
            else
                return null;
        }
    }
}