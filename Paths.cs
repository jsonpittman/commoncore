﻿using System;

namespace CommonCore
{
    public static class Paths
    {
        public static string CallingPath
        {
            get
            {
                return Environment.CurrentDirectory;
            }
        }
        public static string CurrentExecutingDirectoryPath
        {
            get
            {
                return AppContext.BaseDirectory;
            }
        }

        public static string CurrentExecutingFileName
        {
            get
            {
                return System.AppDomain.CurrentDomain.FriendlyName;
            }
        }
    }
}